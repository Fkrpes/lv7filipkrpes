﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lv7zad3
{
    class Program
    {
        static void Main(string[] args)
        {
            SystemDataProvider pcDataProvider = new SystemDataProvider();
            FileLogger fileLogger = new FileLogger("test.txt");
            ConsoleLogger consoleLogger = new ConsoleLogger();
            pcDataProvider.Attach(fileLogger);
            pcDataProvider.Attach(consoleLogger);
            for (; ; )
            {
                pcDataProvider.GetCPULoad();
                pcDataProvider.GetAvailableRAM();
                System.Threading.Thread.Sleep(1000);
            }

        }
    }
}
