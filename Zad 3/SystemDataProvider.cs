﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV7zad3Filipkrpes
{
    class SystemDataProvider:SimpleSystemDataProvider
    {
        private float previousCPULoad;
        private float previousRAMAvailable;
        public SystemDataProvider() : base()
        {
            this.previousCPULoad = this.CPULoad;
            this.previousRAMAvailable = this.AvailableRAM;
        }
        public float GetCPULoad()
        {
            float currentLoad = this.CPULoad;
            //zad 4.samo se if mjenja
            //if((currentLoad <= (this.previousCPULoad * 0.9)||((currentLoad>=this.previousCPULoad * 1.1))
            if (currentLoad != this.previousCPULoad)
            {
                this.Notify();
            }
            this.previousCPULoad = currentLoad;
            return currentLoad;
        }
        public float GetAvailableRAM()
        {
            float currentLoad = this.AvailableRAM;
            //4.zad
            //if((this.previousRAMAvailable*1.1>=currentLoad)||currentLoad <= (this.previousRAMAvailable*0.9))
            if (currentLoad != this.AvailableRAM)
            {
                this.Notify();
            }
            this.previousRAMAvailable = currentLoad;
            return currentLoad;
        }
    }
}
