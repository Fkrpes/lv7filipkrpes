﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lv7zad2Filipkrpes
{
    abstract class SearchStrategy
    {
        public abstract int Search(double[]array, double x);
    }
}
