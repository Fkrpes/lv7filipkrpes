﻿using System;

namespace LV7zad3Filipkrpes
{
    class Program
    {
        static void Main(string[] args)
        {
            SystemDataProvider pcDataProvider = new SystemDataProvider();
            FileLogger fileLogger = new FileLogger("test.txt");
            ConsoleLogger consoleLogger = new ConsoleLogger();
            pcDataProvider.Attach(fileLogger);
            pcDataProvider.Attach(consoleLogger);
            for (; ; )
            {
                pcDataProvider.GetCPULoad();
                pcDataProvider.GetAvailableRAM();
                System.Threading.Thread.Sleep(1000);
            }
            
        }
    }
}
