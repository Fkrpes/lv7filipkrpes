﻿using System;

namespace LV7zad1FilipKrpes
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] testArray= { 2, 123, 1, 312, 4, 1, 7, 5, 23, 89, 45, 2, 34, 46, 234, 12 };

            NumberSequence testSequence = new NumberSequence(testArray);
            BubbleSort bubblesort = new BubbleSort();
            CombSort combsort = new CombSort();
            SequentialSort sequentialsort = new SequentialSort();
           
            testSequence.SetSortStrategy(bubblesort);
            Console.WriteLine("Array befor sort\n"+testSequence.ToString());
            testSequence.Sort();
            Console.WriteLine("Array after bubble sort\n"+testSequence.ToString());

            testSequence = new NumberSequence(testArray);
            testSequence.SetSortStrategy(combsort);
            testSequence.Sort();
            Console.WriteLine("Array after comb sort\n"+testSequence.ToString());

            testSequence = new NumberSequence(testArray);
            testSequence.SetSortStrategy(sequentialsort);
            testSequence.Sort();
            Console.WriteLine("Array after sequential sort\n"+testSequence.ToString());



        }
    }
}
