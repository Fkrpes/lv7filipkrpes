﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lv7zad2Filipkrpes
{
    class BubbleSort : SortStrategy
    {
        public override void Sort(double[] array)
        {
            int i, j, arraySize = array.Length;
            for (i = 0; i < arraySize - 1; i++)
                for (j = 0; j < arraySize - i - 1; j++)
                    if (array[j] > array[j + 1])
                        Swap(ref array[j], ref array[j + 1]);
        }
    }
}
