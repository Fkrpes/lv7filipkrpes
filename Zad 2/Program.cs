﻿using System;

namespace Lv7zad2Filipkrpes
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] testArray = { 2, 123, 1, 312, 4, 1, 7, 5, 23, 89, 45, 2, 34, 46, 234, 12 };
            double x1 = 69;
            double x2 = 7;
            NumberSequence testSequence = new NumberSequence(testArray);
            BubbleSort bubblesort = new BubbleSort();
            CombSort combsort = new CombSort();
            SequentialSort sequentialsort = new SequentialSort();

            testSequence.SetSortStrategy(bubblesort);
            Console.WriteLine("Array befor sort\n" + testSequence.ToString());
            
            LinearSearch linearsearch = new LinearSearch();
            testSequence.SetSsearchStrategy(linearsearch);
            if (testSequence.Search(x1) != -1)
                Console.WriteLine("The number " + x1 + "is on the "+testSequence.Search(7).ToString() + "th place in the array\n");
            else
                Console.WriteLine("The nuber "+ x1 + " does not exit in that array\n");

            if (testSequence.Search(x2) != -1)
                Console.WriteLine("The number " + x2 + " is on the " + testSequence.Search(7).ToString() + "th place in the array\n");
            else
                Console.WriteLine("The nuber " + x2 + " does not exit in that array\n");

            testSequence.Sort();
            Console.WriteLine("Array after bubble sort\n" + testSequence.ToString());

            testSequence = new NumberSequence(testArray);
            testSequence.SetSortStrategy(combsort);
            testSequence.Sort();
            Console.WriteLine("Array after comb sort\n" + testSequence.ToString());

            testSequence = new NumberSequence(testArray);
            testSequence.SetSortStrategy(sequentialsort);
            testSequence.Sort();
            Console.WriteLine("Array after sequential sort\n" + testSequence.ToString());

        }
    }
}
